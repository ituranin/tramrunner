package app.amazing.tramrunner.vbb;

import android.app.Activity;
import android.content.res.Resources;
import android.widget.ArrayAdapter;

import com.loopj.android.http.*;

import java.util.ArrayList;
import java.util.Date;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.vbb.vbbdata.Departure;
import app.amazing.tramrunner.vbb.vbbdata.Station;

/**
 * The VbbApiHelper contains all methods needed to get data from the vbb api
 */
public class VbbApiHelper {
    private static VbbApiHelper ourInstance = new VbbApiHelper();

    private static AsyncHttpClient clientAsync;
    private static SyncHttpClient clientSync;
    private static Activity activity;

    public static VbbApiHelper getInstance() {
        return ourInstance;
    }

    private VbbApiHelper() {
        clientAsync = new AsyncHttpClient();
        clientSync = new SyncHttpClient();
    }

    public static Activity getActivity() {
        return activity;
    }

    public static void setActivity(Activity activity) {
        VbbApiHelper.activity = activity;
    }

    /**
     * This method sets http headers for the testserver
     */
    public static void setHeader() {
        clientAsync.addHeader(ResData.r.getString(R.string.vbb_api_x_identifier), ResData.r.getString(R.string.vbb_api_x_identifier_value));
        clientSync.addHeader(ResData.r.getString(R.string.vbb_api_x_identifier), ResData.r.getString(R.string.vbb_api_x_identifier_value));
    }

    /**
     * This method gets all stations in in a 1000m radius
     * @param latitude the latitude of a position
     * @param longitude the longitude of a position
     * @param stops the adapter where to stations should be written to to be shown in the corresponding dropdown
     */
    public void getStationsNearby(String latitude, String longitude, ArrayAdapter<Station> stops) {
        RequestParams params = new RequestParams();
        params.put(ResData.r.getString(R.string.vbb_api_param_latitude), latitude);
        params.put(ResData.r.getString(R.string.vbb_api_param_longitude), longitude);
        clientAsync.get(ResData.r.getString(R.string.vbb_api_stations_nearby_link), params, new StationsResponseHandler(stops));
    }

    /**
     * This is the synchronous version of getStationsNearby(..).
     * It is mainly used to get all stations near a station specified by the user.
     * It is used in the NextStationTask.
     * Never call this directly from the UI thread!
     * @param latitude the latitude of a position
     * @param longitude the longitude of a position
     * @param departure the departure specified by the user
     * @param requestTime the time of the first departure request made by user to be accurate
     * @param nextStation variable to save the next station
     * @param nextDeparture variable to save the departure at the next station
     */
    public void getNextStationSync(String latitude, String longitude, Departure departure, Date requestTime, Station nextStation, Departure nextDeparture) {
        RequestParams params = new RequestParams();
        params.put(ResData.r.getString(R.string.vbb_api_param_latitude), latitude);
        params.put(ResData.r.getString(R.string.vbb_api_param_longitude), longitude);
        clientSync.get(ResData.r.getString(R.string.vbb_api_stations_nearby_link), params, new StationsResponseHandler(departure, requestTime, nextStation, nextDeparture));
    }

    /**
     * This method is used to start a series of synchronous requests in a AsyncTask to get the next station.
     * @param latitude the latitude of a position
     * @param longitude the longitude of a position
     * @param departure the departure specified by the user
     * @param requestTime the time of the first departure request made by user to be accurate
     */
    public void getNextStation(String latitude, String longitude, Departure departure, Date requestTime) {
        NextStationTask task = new NextStationTask(latitude, longitude, departure, requestTime);
        task.execute();
    }

    /**
     * This method gets all departures from the specified station
     * @param station the station to get all departures from
     * @param requestTime the time of this request
     * @param departures the adapter to write the departures to to show them in a corresponding spinner
     */
    public void getDepartures(String station, Date requestTime, ArrayAdapter<Departure> departures) {
        RequestParams params = new RequestParams();
        String finalUrl = String.format(ResData.r.getString(R.string.vbb_api_departures_link), station);
        params.put(ResData.r.getString(R.string.vbb_api_param_when), requestTime.getTime() / 1000L);
        params.put(ResData.r.getString(R.string.vbb_api_param_duration), ResData.r.getString(R.string.vbb_api_param_duration_value));
        clientAsync.get(finalUrl, params, new DeparturesResponseHandler(departures));
    }

    /**
     * This method gets all departures from the specified station.
     * It is used to get departures when the next station is being determined
     * Never call this directly from the UI thread!
     * @param station the station to get all departures from
     * @param requestTime the time of the first departure request by user
     * @param departures the list where the departures should be written to
     */
    public void getDeparturesSynchronous(String station, Date requestTime, ArrayList<Departure> departures) {
        RequestParams params = new RequestParams();
        String finalUrl = String.format(ResData.r.getString(R.string.vbb_api_departures_link), station);
        params.put(ResData.r.getString(R.string.vbb_api_param_when), requestTime.getTime() / 1000L);
        params.put(ResData.r.getString(R.string.vbb_api_param_duration), ResData.r.getString(R.string.vbb_api_param_duration_value));
        clientSync.get(finalUrl, params, new DeparturesResponseHandler(departures));
    }
}
