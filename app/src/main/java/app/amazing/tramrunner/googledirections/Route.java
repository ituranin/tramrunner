package app.amazing.tramrunner.googledirections;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Delia on 20.01.2017.
 */

/**
 * Klasse mit allen Elementen, die zu einer Route gehören
 */
public class Route {
    public Distance distance;
    public Duration duration;
    //public String endAddress;
    public LatLng endLocation;
    //public String startAddress;
    public LatLng startLocation;

    public String encodedPolyline;
}
