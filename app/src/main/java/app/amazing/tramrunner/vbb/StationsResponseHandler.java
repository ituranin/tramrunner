package app.amazing.tramrunner.vbb;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.vbb.vbbdata.Departure;
import app.amazing.tramrunner.vbb.vbbdata.Station;
import cz.msebera.android.httpclient.Header;

/**
 * This handler handles the response for stations
 */

public class StationsResponseHandler extends TextHttpResponseHandler {

    private ArrayAdapter<Station> stops = null;
    private Departure departure = null;
    private Date requestTime = null;
    private boolean async = true;
    private Station nextStation = null;
    private Departure nextDeparture = null;

    public StationsResponseHandler(ArrayAdapter<Station> stops) {
        this.stops = stops;
    }
    public StationsResponseHandler(Departure departure, Date requestTime, Station nextStation, Departure nextDeparture) {
        async = false;
        this.departure = departure;
        this.requestTime = requestTime;
        this.nextStation = nextStation;
        this.nextDeparture = nextDeparture;
    }
    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        Log.d("[VBB]", "failure!");
        Log.d("[VBB]", responseString);
        if(stops != null) {
            stops.clear();
            stops.add(new Station(ResData.r.getString(R.string.vbb_api_itemcode_network_error)));
        }
        if(!async) {
            nextStation.copy(new Station(ResData.r.getString(R.string.vbb_api_itemcode_network_error)));
            nextDeparture.copy(new Departure(ResData.r.getString(R.string.vbb_api_itemcode_network_error)));
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String responseString) {
        Log.d("[VBB]", "success!");
        Log.d("[VBB]", responseString);
        if (async) {
            OwnJSONParser.getStopLocationList(responseString, stops);
        } else {
            ArrayList<Station> stations = OwnJSONParser.getStopLocationAsList(responseString);
            Iterator<Station> it = stations.iterator();
            boolean departureFound = false;
            Station nextStation = null;
            Departure nextStationDeparture = null;

            // skip first station result
            if (it.hasNext()) {
                it.next();
            }

            while (it.hasNext()) {
                Station temp = it.next();
                ArrayList<Departure> tempDepartures = new ArrayList<Departure>();
                VbbApiHelper.getInstance().getDeparturesSynchronous(temp.getId(), requestTime, tempDepartures);
                Iterator<Departure> depIterator = tempDepartures.iterator();
                //Log.d("[COMP WITH]: ", departure.toString());
                while (depIterator.hasNext()) {
                    Departure tempDeparture = depIterator.next();
                    //Log.d("[DEPARTURE]: ", tempDeparture.toString());
                    if (tempDeparture.getTrip().equalsIgnoreCase(departure.getTrip())) {
                        if (tempDeparture.getTimestamp() > departure.getTimestamp() || tempDeparture.getWhen().equalsIgnoreCase(departure.getWhen())) {
                            departureFound = true;
                            nextStationDeparture = tempDeparture;
                        }
                    }

                    if (departureFound) {
                        break;
                    }
                }

                if (departureFound) {
                    //Log.d("[FOUND DEPARTURE]: ", "yey!");
                    nextStation = temp;
                    break;
                }
            }

            if(nextStation == null) {
                nextStation = new Station(ResData.r.getString(R.string.vbb_api_itemcode_not_found));
            }

            if(nextStationDeparture == null) {
                nextStationDeparture = new Departure(ResData.r.getString(R.string.vbb_api_itemcode_not_found));
            }

            this.nextStation.copy(nextStation);
            this.nextDeparture.copy(nextStationDeparture);
        }
    }
}
