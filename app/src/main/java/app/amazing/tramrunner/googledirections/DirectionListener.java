package app.amazing.tramrunner.googledirections;

import java.util.List;

import app.amazing.tramrunner.googledirections.Route;

/**
 * Created by Delia on 21.01.2017.
 */

public interface DirectionListener {
    void onDirectionRequestSuccess(List<Route> routes);
}
