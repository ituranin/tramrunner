package app.amazing.tramrunner.vbb;

import android.os.AsyncTask;

import java.util.Date;

import app.amazing.tramrunner.MainActivity;
import app.amazing.tramrunner.vbb.vbbdata.Departure;
import app.amazing.tramrunner.vbb.vbbdata.Station;

/**
 * This AsyncTask gets the next station in a new thread.
 */

public class NextStationTask extends AsyncTask {

    private String latitude;
    private String longitude;
    private Departure departure;
    private Date requestTime;
    private Station nextStation = new Station();
    private Departure nextDeparture = new Departure();

    public NextStationTask(String latitude, String longitude, Departure departure, Date requestTime) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.departure = departure;
        this.requestTime = requestTime;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        VbbApiHelper.getInstance().getNextStationSync(latitude, longitude, departure, requestTime, nextStation, nextDeparture);
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        MainActivity activity = (MainActivity) VbbApiHelper.getInstance().getActivity();
        activity.setStartButtonValues(nextStation, nextDeparture);
    }
}
