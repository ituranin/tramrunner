package app.amazing.tramrunner.googledirections;

/**
 * Created by Delia on 21.01.2017.
 */

/**
 * Klasse fuer die Entfernung einer Strecke
 */
public class Distance {
    private String text;
    private int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }
}
