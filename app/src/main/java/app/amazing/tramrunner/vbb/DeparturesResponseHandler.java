package app.amazing.tramrunner.vbb;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.vbb.vbbdata.Departure;
import cz.msebera.android.httpclient.Header;

/**
 * This handler handls the response for departures
 */

public class DeparturesResponseHandler extends TextHttpResponseHandler {

    private ArrayAdapter<Departure> departures = null;
    private ArrayList<Departure> departuresArray = null;
    private boolean async = true;

    public DeparturesResponseHandler(ArrayAdapter<Departure> departures) {
        this.departures = departures;
    }

    public DeparturesResponseHandler(ArrayList<Departure> departures) {
        this.departuresArray = departures;
        this.async = false;
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        if(async) {
            departures.clear();
            departures.add(new Departure(ResData.r.getString(R.string.vbb_api_itemcode_network_error)));
        } else {
            departuresArray.clear();
            departuresArray.add(new Departure(ResData.r.getString(R.string.vbb_api_itemcode_network_error)));
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String responseString) {
        //Log.d("[VBB]", "success!");
        //Log.d("[VBB]", responseString);
        if (async) {
            OwnJSONParser.getDepartures(responseString, departures);
        } else {
            OwnJSONParser.getDeparturesToList(responseString, departuresArray);
        }
    }
}
