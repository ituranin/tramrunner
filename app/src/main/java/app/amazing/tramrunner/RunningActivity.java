package app.amazing.tramrunner;


import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import app.amazing.tramrunner.fragments.RunActiveProgressFragment;

import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import app.amazing.tramrunner.location.LocationContainer;

import static com.google.maps.android.PolyUtil.decode;

public class RunningActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap map;
    private RunActiveProgressFragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);

        fragment = (RunActiveProgressFragment) getFragmentManager().findFragmentById(R.id.fragment);

        //maps stuff from here
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);

        //LocationContainer.getLocationOnes(this);
        LatLng myPos = new LatLng(LocationContainer.getLastKnownLocation().getLatitude(), LocationContainer.getLastKnownLocation().getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(myPos, 15));

        setDestinationMarker();
        drawRoute();

    }

    /**
     * method to set a marker at the destination-station
     */
    private void setDestinationMarker() {
        String destinationTitle = getIntent().getStringExtra(getString(R.string.extra_nextStation));
        double stationLat = getIntent().getExtras().getDouble(getString(R.string.extra_stationLat));
        double stationLng = getIntent().getExtras().getDouble(getString(R.string.extra_stationLon));
        LatLng destinationMarker = new LatLng(stationLat, stationLng);
        map.addMarker(new MarkerOptions().position(destinationMarker).title(destinationTitle));
    }

    /**
     * method to draw the route on the map
     */
    private void drawRoute() {
        PolylineOptions routeOptions = new PolylineOptions().color(ContextCompat.getColor(this, R.color.colorPrimary));
        String encodedRoute = getIntent().getStringExtra(getString(R.string.extra_polyline));
        List<LatLng> routes = decode(encodedRoute);
        //builder to set bounds
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng point : routes) {
            //add the points to the route
            routeOptions.add(point);
            //get all points, which are inside the bounds
            builder.include(point);
        }

        map.addPolyline(routeOptions);
        LatLngBounds bounds = builder.build();
        //bounds padding
        int padding = 50;
        CameraUpdate updateBounds = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.moveCamera(updateBounds);
    }

    /**
     * pop-up which asks the user, if he really wants to abort the run
     */
    private void abortRun_PopUp() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.abort_run_question));
        //Confirm - Run aborted, back to MainActivity
        builder.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(RunningActivity.this, getString(R.string.abort_run_confiramtion), Toast.LENGTH_SHORT).show();
                fragment.removeRunningListener();
                finish();
                dialog.dismiss();
            }
        });
        //Cancel - back to the run
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void onButtonClick(View v) {
        abortRun_PopUp();
    }

    @Override
    public void onBackPressed() {
        abortRun_PopUp();
    }
}
