package app.amazing.tramrunner.googledirections;


/**
 * Created by Delia on 20.01.2017.
 */

import android.location.Location;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.vbb.vbbdata.Station;

/**
 * Klasse zum Erstellen der Anfrage
 */
public class MapsDirectionAPI {

    protected static String DIRECTIONS_API_URL;
    protected static String DIRECTIONS_API_KEY;

    private DirectionListener listener;
    private Location origin;
    private Station destination;

    public MapsDirectionAPI(DirectionListener l, Location o, Station d) {
        this.listener = l;
        this.origin = o;
        this.destination = d;
        DIRECTIONS_API_URL = ResData.r.getString(R.string.directions_api_url);
        DIRECTIONS_API_KEY = ResData.r.getString(R.string.google_directions_key);
    }

    /**
     * Ausführen einer Anfrage
     */
    public void execute() {
        new DirectionRequest(listener).execute(createURL());
    }

    /**
     * erstellen einer URL für die Anfrage
     *
     * @return URL
     */
    public String createURL() {
        String originString = String.format(ResData.r.getString(R.string.origin_string), String.valueOf(origin.getLatitude()), String.valueOf(origin.getLongitude()));
        String destinationString = String.format(ResData.r.getString(R.string.destination_string), destination.getLatitude(), destination.getLongitude());

        System.out.println(String.format(DIRECTIONS_API_URL, originString, destinationString, DIRECTIONS_API_KEY));
        return String.format(DIRECTIONS_API_URL, originString, destinationString, DIRECTIONS_API_KEY);
    }


}
