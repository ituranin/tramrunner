package app.amazing.tramrunner.location;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.Date;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.data.StatisticData;
import app.amazing.tramrunner.runningProgress.RunningProgressbar;

/**
 * Created by igor on 24.01.17.
 */

public class RunningLocationListener implements LocationListener {

    private Activity view;
    private Location destination;
    private float metersToRun;
    private long departureTime;
    private RunningProgressbar bar;
    private float distance;
    private float distanceRan;
    private float progressRunner;
    private float progressTram;
    private long timeToDrive;
    private long timeDriven;
    private long tramTimeDiff;

    public RunningLocationListener(Activity view, double latitude, double longitude, long departureTime, RunningProgressbar bar) {
        this.view = view;
        destination = new Location(LocationManager.GPS_PROVIDER);
        destination.setLatitude(latitude);
        destination.setLongitude(longitude);
        this.departureTime = departureTime;
        timeToDrive = departureTime - new Date().getTime();
        this.bar = bar;
        metersToRun = destination.distanceTo(LocationContainer.getLastKnownLocation());
        distance = LocationContainer.getLastKnownLocation().distanceTo(destination);
    }

    @Override
    public void onLocationChanged(Location location) {

        distance = location.distanceTo(destination);

        // calculate progresses
        distanceRan = metersToRun - distance;
        progressRunner = distanceRan / metersToRun;
        tramTimeDiff = departureTime - new Date().getTime();
        timeDriven = timeToDrive - tramTimeDiff;
        progressTram = (float) ((double) timeDriven / (double) timeToDrive);

        if (progressRunner > 1.0f) {
            progressRunner = 1.0f;
        }

        if (progressTram > 1.0f) {
            progressTram = 1.0f;
        }

        // update progresses
        bar.updateRunnerProgress(progressRunner);
        bar.updateTramProgress(progressTram);

        if (distance < 10.0f) {
            try {
                LocationContainer.getLocationManager().removeUpdates(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }

            if (new Date().getTime() <= departureTime) {
                StatisticData.getInstance().addSuccessfulRunData((int) metersToRun);

                AlertDialog.Builder builder = new AlertDialog.Builder(view);
                builder.setTitle(ResData.r.getString(R.string.you_won));
                builder.setPositiveButton(ResData.r.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        view.finish();
                        dialog.dismiss();
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        view.finish();
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                if (!view.isFinishing()) {
                    dialog.show();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(view);
                builder.setTitle(ResData.r.getString(R.string.you_lost));
                builder.setPositiveButton(ResData.r.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        view.finish();
                        dialog.dismiss();
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        view.finish();
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                if (!view.isFinishing()) {
                    dialog.show();
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        //Log.d("[ENABLED]", "PROVIDER");
    }

    @Override
    public void onProviderDisabled(String provider) {

        AlertDialog.Builder builder = new AlertDialog.Builder(view);
        builder.setTitle(ResData.r.getString(R.string.enable_gps));
        builder.setPositiveButton(ResData.r.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                view.finish();
                dialog.dismiss();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                view.finish();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        if (!view.isFinishing()) {
            dialog.show();
        }
        //Log.d("[DISABLED]", "PROVIDER");
    }
}
